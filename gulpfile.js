var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var minifyCSS 	= require("gulp-minify-css");
// var minifyJS 	= require("gulp-uglify"); Not needed, js-files are already compressed
var imagemin = require('gulp-imagemin');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var replace = require('gulp-replace');

//########## START DEVELOP SECTION ###############

//Copy html-files to tmp-folder
gulp.task('html', function () {
    return gulp.src(['src/**/*.html'])
        .pipe(gulp.dest('tmp'));
});

//Copy image-folder from src to tmp-folder
gulp.task('dev-images', function () {
    return gulp.src(['src/images/**'])
        .pipe(gulp.dest('tmp/images'));
});

// Copy the javascript files from node-folder into /tmp/js folder
gulp.task('js', function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest("tmp/js"))
        .pipe(browserSync.stream());
});

// Compile sass-file from src/scss into tmp/css-folder & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src(['src/scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("tmp/css"))
        .pipe(browserSync.stream());
});

// Task for starting the server and watch for changes in the tmp-files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        browser: ['google-chrome','firefox'],
        server: "./tmp"
    });

    gulp.watch(['src/**/*.html'], ['html']);
    gulp.watch(['src/scss/*.scss'], ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

// Default Task for Development, simply call it with command "gulp"
gulp.task('default', ['html','dev-images','js','serve']);


//########## END DEVELOP SECTION ###############


//########## START DISTRIBUTION SECTION ###############

//Delete dist-folder for fresh distribution
gulp.task('clean-dist', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

// Delete tmp-folder for fresh development
gulp.task('clean-tmp', function () {
    return gulp.src('tmp', {read: false})
        .pipe(clean());
});

//Copy html-files from tmp to dist-folder
gulp.task('html2', function () {
    return gulp.src(['tmp/**/*.html'])
        .pipe(replace('css/custom.css', 'css/custom.min.css'))
        .pipe(gulp.dest('dist'));
});

// Minify, rename and Copy CSS-file from tmp/css to dist/css-folder
gulp.task('compressCSS', function () {
    gulp.src('tmp/css/*.css') // path to your file
    .pipe(minifyCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/css'));
});

// copy js-files from tmp/js to dist/js-folder
gulp.task('copyJS', function () {
    gulp.src('tmp/js/*.js') // path to your file
    .pipe(gulp.dest('dist/js'));
});

// Optimize Size and copy all images from /tmp/images to dist/images-folder
gulp.task('dist-images', function () {
    gulp.src('tmp/images/**')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
});

// Task-Sequence for creating the content of the dist-foler
gulp.task('min-files', function(cb) {
    runSequence('clean-dist',['html2','compressCSS','copyJS','dist-images'], cb);
});

// Default Task for Distribution, simply call it with command "gulp handout"
gulp.task('handout', ['min-files'], function() {

    browserSync.init({
        browser: ['google-chrome','firefox'],
        server: "./dist"
    });
});

//########## END DISTRIBUTION SECTION ###############

// Reset-Task to delete tmp- and dist-folder
gulp.task('reset', ['clean-tmp', 'clean-dist']);
